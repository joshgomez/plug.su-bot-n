
var fs = require('fs');
var path = require('path'); 
var request = require('request');

var PlugAPI = require('plugapi');
var Cleverbot = require('cleverbot-node');

//--------------------------------------------------------------------------
//Utilities
//--------------------------------------------------------------------------
var Utility = {}

Utility.merge = function()
{
    var destination = {},
        sources = [].slice.call( arguments, 0 );
    sources.forEach(function( source ) {
        var prop;
        for ( prop in source ) {
            if ( prop in destination && Array.isArray( destination[ prop ] ) ) {
                
                // Concat Arrays
                destination[ prop ] = destination[ prop ].concat( source[ prop ] );
                
            } else if ( prop in destination && typeof destination[ prop ] === "object" ) {
                
                // Merge Objects
                destination[ prop ] = Utility.merge( destination[ prop ], source[ prop ] );
                
            } else {
                
                // Set new values
                destination[ prop ] = source[ prop ];
                
            }
        }
    });
    return destination;
};

Utility._objectToArray = function(object,key)
{
	
		var key = (key) ? true : false;
		var arr = [];
		for(var index in object)
		{
			if(key)
			{
				arr.push(index);
				continue;
			}
			
			arr.push(object[index]);
		}
		
		return arr;
};
	
Utility.objectToPagedArray = function(object,page,max,key)
{
	var arr = Utility._objectToArray(object,key);
	
	var maxPage = Math.ceil(arr.length/max);
	if(page > maxPage)
		page = maxPage;
	
	var end = max*page;
	
	return [arr.slice(max*(page - 1),end),page,maxPage];
};

Utility.toInt = function(str)
{
	var number = parseInt(str);
	if(isNaN(number) === false)
		return number;
		
	return 0;
};

//--------------------------------------------------------------------------
//Main Object
//--------------------------------------------------------------------------
function PlugSuBot(bot,config,language)
{
	this.addins = []
	
	this._config 	= config 	|| {};
	this._language 	= language 	|| {}; 
	
	this._author 	= 'Josh aka XzaR';
	this._version 	= '1.3.1';
	this._url 		= 'http://www.xuniver.se';
	this._srcUrl 	= 'https://bitbucket.org/joshgomez/plug.su-bot-n';
	
	this._config.maximumTrackLength = this._config.maximumTrackLength 	|| 600;
	this._config.lengthBeforeSkip 	= this._config.lengthBeforeSkip 	|| 180;
	this._config.language 			= this._config.language 			|| "en";
	this._config.roomDisplayName 	= this._config.roomDisplayName 		|| "Sound Universe";
	
	this._bot 	= bot;
	this._room 	= this._config.Credential.room || "sound-universe";
	
	this._chatBuffer 					= [];
	this._callBuffer 					= [];
	
	this._timers 						= {};
	this._timers.sendChat 				= [];
	this._timers.skip 					= null;
	this._timers.skipMessage 			= null;
	this._timers.vote 					= null;
	this._timers.connect 				= null;
	this._timers.welcome 				= [];
	this._timers.call 					= null;
	
	this._voteNegative = false;
	
	this._init();

	return this;
}

PlugSuBot.Addins = {};

//--------------------------------------------------------------------------
//Addin Chatter
//--------------------------------------------------------------------------


PlugSuBot.Addins.Chatter = function(base,provider)
{
	this._base 		= base;
	this._Provider 	= provider;
	this._instance 	= new this._Provider();
}

PlugSuBot.Addins.Chatter.prototype._sendResponse = function(str,target)
{
	if(this._Provider == null)
	{
		return;
	}
	
	var self = this;
	this._Provider.prepare(function()
	{
		self._instance.write(str, function (response)
		{
			self._base._sendChat("@" + target + " " + response.message);
		});
	});
};

PlugSuBot.Addins.Chatter.prototype.onCommand = function(data,user,bot)
{
	var arg = "";
	
	arg = this._base._messageToCommand('@' + bot.username,data.message);
	if(arg !== "" && user.username !== bot.username)
	{
		this._sendResponse(arg,user.username);
		return "";
	}
	
	return "";
}

//--------------------------------------------------------------------------
//Addin APIChuckNorrisJokes
//--------------------------------------------------------------------------

PlugSuBot.Addins.APIChuckNorrisJokes = function(base)
{
	this._base 		= base;
	this._endpoint 	= "http://api.icndb.com/jokes/random";
	
	this._base._callBuffer.push({self:this,func:this._sendResponse});
}

PlugSuBot.Addins.APIChuckNorrisJokes.prototype._sendResponse = function()
{
	var self = this;
	request(this._endpoint, function (error, response, body)
	{
		if (!error && response.statusCode == 200)
		{
			var data = JSON.parse(body);
			var message = data.value.joke;
			message = message.replace(/&quot;/g,'"');
			self._base._sendChat(message);
		}
	});
};

//--------------------------------------------------------------------------
//Addin APICatFacts
//--------------------------------------------------------------------------

PlugSuBot.Addins.APICatFacts = function(base)
{
	this._base 		= base;
	this._endpoint 	= "http://catfacts-api.appspot.com/api/facts"
	
	this._base._callBuffer.push({self:this,func:this._sendResponse});
}

PlugSuBot.Addins.APICatFacts.prototype._sendResponse = function()
{
	var self = this;
	request(this._endpoint, function (error, response, body)
	{
		if (!error && response.statusCode == 200)
		{
			var data = JSON.parse(body);
			self._base._sendChat(data.facts[0]);
		}
	});
};

//--------------------------------------------------------------------------
//Addin AutoMessages
//--------------------------------------------------------------------------

PlugSuBot.Addins.AutoMessages = function(base)
{
	this._base 		= base;
	this._language 	= require('./localization/AutoMessages.en.json') || {};
	
	var languagePath = './localization/AutoMessages.' + path.basename(this._base._config.language) + '.json'
	if (this._base._config.language !== "en" && fs.existsSync(languagePath))
	{ 
		this._language = Utility.merge(require(languagePath) || {}, this._language);
	} 
	
	this._base._callBuffer.push({self:this,func:this._sendResponse});
}

PlugSuBot.Addins.AutoMessages.prototype._sendResponse = function()
{
	var messages = this._language[this._base._config.language];
	if(typeof messages != 'undefined')
	{
		var messages = this._language["en"];
	}
	
	var str = this._base._translate(this._language,Math.floor(Math.random() * messages.length));
	this._base._sendChat(str);
};

//--------------------------------------------------------------------------
//Addin QuestionAndAnswers
//--------------------------------------------------------------------------

PlugSuBot.Addins.QuestionAndAnswers = function(base)
{
	this._base 		= base;
	this._language 	= require('./localization/QuestionAndAnswers.en.json') || {};
	
	var languagePath = './localization/QuestionAndAnswers.' + path.basename(this._base._config.language) + '.json'
	if (this._base._config.language !== "en" && fs.existsSync(languagePath))
	{ 
		this._language = Utility.merge(require(languagePath) || {}, this._language);
	}
	
	this.timerAnswer = null;
	
	this._base._callBuffer.push({self:this,func:this._sendResponse});
}

PlugSuBot.Addins.QuestionAndAnswers.prototype._sendResponse = function()
{
	var messages = this._language[this._base._config.language];
	if(typeof messages != 'undefined')
	{
		var messages = this._language["en"];
	}
	
	var index = Math.floor(Math.random() * messages.length);
	var message = messages[index];
	this._base._sendChat(message.question);
	
	if(typeof message.answer != 'undefined')
	{
		if(this.timerAnswer)
			clearTimeout(this.timerAnswer);
	
		var self = this;
		this.timerAnswer = setTimeout(function()
		{
			self._base._sendChat(message.answer);
			
		},15*1000);
	}
};

//--------------------------------------------------------------------------
//Addin Ads
//--------------------------------------------------------------------------

PlugSuBot.Addins.Ads = function(base)
{
	this._base 		= base;
	this._language 	= require('./localization/Ads.en.json') || {};
	
	var languagePath = './localization/Ads.' + path.basename(this._base._config.language) + '.json'
	if (this._base._config.language !== "en" && fs.existsSync(languagePath))
	{ 
		this._language = Utility.merge(require(languagePath) || {}, this._language);
	}
	
	var bot = this._base._bot.getSelf();
	this._botname = bot.username;
	
	this._timerAds = null;
	this._sendResponses();
}

PlugSuBot.Addins.Ads.prototype._sendResponses = function()
{
	var messages = this._language[this._base._config.language];
	if(typeof messages != 'undefined')
	{
		var messages = this._language["en"];
	}
	
	var index = 0;
	var self = this;
	this._timerAds = setInterval(function()
	{
		if(messages.length === 0)
		{
			return;
		}
		
		var str = self._base._translate(self._language,index);
		str = str.replace('%botname%',self._botname);
		str = str.replace('%CMD_HELP%',self._base._c("CMD_HELP"));
		str = str.replace('%CMD_SOURCE%',self._base._c("CMD_SOURCE"));
		self._base._sendChat(str);
		
		index++;
		if(index >= messages.length)
		{
			index = 0;
		}
		
	},8*60*1000);
};

//--------------------------------------------------------------------------
//Public Methods - TODO: refactor the name
//--------------------------------------------------------------------------
PlugSuBot.prototype._translate = function(obj,key)
{
	var lang = obj[this._config.language];
	if(lang && lang[key])
		return lang[key];
	
	return obj['en'][key];
};

PlugSuBot.prototype._t = function(key)
{
	return this._translate(this._language,key)
};

PlugSuBot.prototype._c = function(key)
{
	return "!" + this._t(key);
};

PlugSuBot.prototype._sendChat = function(str)
{
	if(typeof str == "undefined" || str == "")
	{
		return;
	}	
	
	var index = this._chatBuffer.push(str);
		
	if(this._timers.sendChat[index]) 
	{
		clearTimeout(this._timers.sendChat[index]);
		this._timers.sendChat.splice(index,1); 
	}
	
	var self = this;
	this._timers.sendChat[index] = setTimeout(function()
	{
		self._timers.sendChat.shift();
		var message = self._chatBuffer.shift();
		self._bot.sendChat(message);
	
		if(self._chatBuffer.length > 0) self._sendChat();
	
	},650*self._chatBuffer.length);
};

PlugSuBot.prototype._messageToCommand = function(needle,haystack)
{
	needle = needle + ' ';
	index = haystack.indexOf(needle);
	if(index === 0)
	{
		return haystack.substr(needle.length);
	}
	
	return "";
};

//--------------------------------------------------------------------------
//Private Methods
//--------------------------------------------------------------------------
PlugSuBot.prototype._init = function()
{
	var self = this;
	this._connect();
	
	var cbIndex = 0;
	this._timers.call = setInterval(function()
	{
		if(self._callBuffer.length === 0)
		{
			return;
		}
		
		var callObj = self._callBuffer[cbIndex];
		if(typeof callObj === 'object')
		{
			callObj.func.call(callObj.self);
		}
		
		cbIndex++;
		if(cbIndex >= self._callBuffer.length)
		{
			cbIndex = 0;
		}
		
	},10*60*1000);
};

PlugSuBot.prototype._tooLong = function(timeRemain,username)
{
	if(this._timers.skip)
		clearTimeout(this._timers.skip);
	
	if(this._timers.skipMessage)
		clearTimeout(this._timers.skipMessage);
	
	if((timeRemain > this._config.maximumTrackLength))
	{
		var lengthBeforeSkip = (this._config.lengthBeforeSkip > 0) ? this._config.lengthBeforeSkip : this._config.maximumTrackLength;
		
		var strTooLong = this._t('TXT_TOO_LONG');
		strTooLong = strTooLong.replace('%username%',username);
		strTooLong = strTooLong.replace('%seconds%',lengthBeforeSkip);
		
		var self = this;
		this._timers.skip = window.setTimeout(function()
		{
			self._bot.moderateForceSkip();
			
		},lengthBeforeSkip*1000);
		
		this._timers.skipMessage = window.setTimeout(function()
		{
			self._sendChat(strTooLong);
			
		},1000);
		
		this._voteNegative = true;
	}
};

PlugSuBot.prototype._inHistory = function(media,username)
{
	var self = this;
	this._bot.getHistory(function(history)
	{
		for(var i in history)
		{
			if(history[i].media == null || i == 0) continue;
		
			if 
			(
				history[i].media.author == media.author && history[i].media.title == media.title
			)
			{
				var strInHistory = self._t('TXT_IN_HISTORY');
				strInHistory = strInHistory.replace('%username%',username);
				self._sendChat(strInHistory);
				self._voteNegative = true;
				self._vote();
				break;
			}
		}
	});
};

PlugSuBot.prototype._vote = function()
{
	var self = this;
	if(this._timers.vote)
		clearTimeout(this._timers.vote);
	
	this._timers.vote = setTimeout(function()
	{
		if(self._voteNegative)
		{
			self._bot.meh();
			return;
		}
		
		self._bot.woot();
		
	},1000*(Math.random()*5+5));
};



PlugSuBot.prototype._connect = function(delay)
{
	var _delay = delay || 1000;
	var self = this;
	
	if(this._timers.connect)
		clearTimeout(this._timers.connect);
	
	this._timers.connect = setTimeout(function()
	{
		self._bot.connect(self._room);
		
	},_delay)
};



PlugSuBot.prototype._getAllCommands = function()
{
	var commands = {}
	
	var language = this._language[this._config.language];
	for(key in language)
	{
		if(key.indexOf("CMD_") === 0)
		{
			commands[key] = language[key];
		}
	}
	
	return commands;
};

//--------------------------------------------------------------------------
//Private Chat Message Methods
//--------------------------------------------------------------------------

PlugSuBot.prototype._sprintCommands = function(page,max)
{
	var page = 	page 	|| 1;
	var max = 	max 	|| 7;
	
	var commands = Utility.objectToPagedArray(this._getAllCommands(),page,max);
	
	var str = '@%username% !' + commands[0].join(', !');
	if(commands[1] < commands[2])
		str += ', ' + this._c("CMD_COMMANDS") + ' ' + (commands[1] + 1);
	
	return str;
};


PlugSuBot.prototype._printLastPlayedTrack = function(lastPlay)
{
	var str = this._t('TXT_PLAYED');
	str = str.replace('%username%',lastPlay.dj.username);
	str = str.replace('%trackauthor%',lastPlay.media.author);
	str = str.replace('%tracktitle%',lastPlay.media.title);
	str = str.replace('%woot%',lastPlay.score.positive);
	str = str.replace('%meh%',lastPlay.score.negative);
	str = str.replace('%love%',lastPlay.score.grabs);
	this._sendChat(str);
};

PlugSuBot.prototype._printUserGrab = function(user,media)
{
	var str = this._t('TXT_GRABBED');
	str = str.replace('%username%',user.username);
	str = str.replace('%trackauthor%', media.author);
	str = str.replace('%tracktitle%',media.title);
	this._sendChat(str);
};

PlugSuBot.prototype._printWelcome = function(user)
{
	var bot 	= this._bot.getSelf()
	
	var str = this._t('TXT_WELCOME');
	str = str.replace('%username%',user.username);
	str = str.replace('%CMD_HELP%',this._c("CMD_HELP"));
	str = str.replace('%room%',this._config.roomDisplayName);
	str = str.replace('%botname%',bot.username);
	this._sendChat(str)
};

//--------------------------------------------------------------------------
//Events
//--------------------------------------------------------------------------
PlugSuBot.prototype.onRoomJoin = function()
{
	this._bindEvents();
	this._bindAddins();
	
	console.log("Joined " + this._room);
};

PlugSuBot.prototype.onError = function()
{
	this._connect(30000);
};

PlugSuBot.prototype.onClose = function()
{
	this._connect(30000);
};

PlugSuBot.prototype.onComplexCommand = function(data,user,bot)
{
	var arg = "";
	
	for(var i = 0;i < this.addins.length;i++)
	{
		if(this.addins[i].onCommand)
		{
			arg = this.addins[i].onCommand(data,user,bot);
			if(arg !== "")
			{
				return arg;
			}
		}
	}
	
	arg = this._messageToCommand(this._c('CMD_COMMANDS'),data.message);
	if(arg !== "")
	{
		var page = Utility.toInt(arg);
		return this._sprintCommands(page);
	}
	
	arg = this._messageToCommand(this._c('CMD_MANUAL'),data.message);
	if(arg !== "")
	{
		var commands = this._getAllCommands();
		for(index in commands)
		{
			if(commands[index] == arg)
			{
				return '@%username% ' + this._t("MAN_" + index.substr(4));
			}
		}
		
		return this._t('TXT_MANUAL_NOT_FOUND');
	}
	
	return "";
};

PlugSuBot.prototype.onChat = function(data)
{
	var user 	= data.from;
	var bot 	= this._bot.getSelf()
	
	var str = "";
	switch(data.message)
	{
		case this._c('CMD_ABOUT'):
			str = this._t('TXT_ABOUT');
			str = str.replace('%author%',this._author);
			str = str.replace('%url%',this._url);
			break;
		case this._c('CMD_HELP'):
			str = this._t('TXT_HELP');
			str = str.replace('%CMD_COMMANDS%',this._c("CMD_COMMANDS"));
			str = str.replace('%CMD_MANUAL%',this._c("CMD_MANUAL"));
			break;
		case this._c('CMD_VERSION'):
			str = this._t('TXT_VERSION');
			str = str.replace('%version%',this._version);
			break;
		case this._c('CMD_SOURCE'):
			str = this._t('TXT_SOURCE');
			str = str.replace('%url%',this._srcUrl);
			break;
		case this._c('CMD_MANUAL'):
			str = this._t('TXT_MANUAL_NOT_ENTERED');
			break;
		case this._c('CMD_COMMANDS'):
			str = this._sprintCommands(1);
			break;
		default:
			str = this.onComplexCommand(data,user,bot);
	}
	
	if(str !== "")
	{
		str = str.replace('%username%',user.username);
		this._sendChat(str);
	}
	
	//console.log("CHAT",data);
};

PlugSuBot.prototype.onAdvance = function(data)
{
	this._voteNegative = false;
	
	var self = this;
	if(data.media)
	{
		var timeRemaining = data.media.duration || 0;
		if(timeRemaining > 0)
		{
			this._tooLong(timeRemaining,data.currentDJ.username)
			this._inHistory(data.media,data.currentDJ.username);
			
			if(timeRemaining > 20)
			{
				this._vote();
			}
		}
	}
	
	if(data.lastPlay && data.lastPlay.media != null)
	{
		var lastPlay = data.lastPlay;
		this._printLastPlayedTrack(lastPlay);
	}
	
	//console.log("ADVANCED",data);
};

PlugSuBot.prototype.onUserLeave = function(user)
{
	var str = this._t('TXT_LEFT');
	str = str.replace('%username%',user.username);
	this._sendChat(str);
};

PlugSuBot.prototype.onUserJoin = function(user)
{
	var str = this._t('TXT_JOINED');
	str = str.replace('%username%',user.username);
	this._sendChat(str);
	
	var self = this;
	var tIndex = this._timers.welcome.push(setTimeout(function()
	{
		self._timers.welcome.splice(tIndex,1);
		self._printWelcome(user);
	},500*this._timers.welcome.length));
};

PlugSuBot.prototype.onUserGrab = function(data)
{
	var user 	= this._bot.getUser(data.id);
	var media 	= this._bot.getMedia();
	this._printUserGrab(user,media);
	
	//console.log("GRABED",data);
};

//--------------------------------------------------------------------------
//AppStart
//--------------------------------------------------------------------------
PlugSuBot.prototype._bindEvents = function()
{
	var self = this;
	
	this._bot.off('close');
	this._bot.off('error');
	this._bot.off('chat');
	this._bot.off('advance');
	this._bot.off('userLeave');
	this._bot.off('userJoin');
	this._bot.off('curateUpdate');
	
	this._bot.on('close', function(){self.onClose();});
	this._bot.on('error',function(){self.onError();});
	this._bot.on('chat',function(data){self.onChat(data);});
	this._bot.on('advance',function(data){self.onAdvance(data);});
	this._bot.on('userLeave',function(data){self.onUserLeave(data);});
	this._bot.on('userJoin',function(data){self.onUserJoin(data);});
	this._bot.on('curateUpdate',function(data){self.onUserGrab(data);});
};

PlugSuBot.prototype._bindAddins = function()
{
	this.addins = [];
	
	this.addins.push(new PlugSuBot.Addins.Chatter(this,Cleverbot));
	this.addins.push(new PlugSuBot.Addins.APIChuckNorrisJokes(this));
	this.addins.push(new PlugSuBot.Addins.APICatFacts(this));
	this.addins.push(new PlugSuBot.Addins.AutoMessages(this));
	this.addins.push(new PlugSuBot.Addins.QuestionAndAnswers(this));
	this.addins.push(new PlugSuBot.Addins.Ads(this));
};

function PlugSuBotNextGen()
{
	var Config 		= require('./Configs.json') 		|| {};
	var Language 	= require('./localization/PlugSuBot.en.json') 	|| {};
	
	var languagePath = './localization/PlugSuBot.' + path.basename(Config.language) + '.json'
	if (Config.language !== "en" && fs.existsSync(languagePath))
	{ 
		Language = Utility.merge(require(languagePath) || {},Language);
	} 
	
	new PlugAPI(
		{
			email: Config.Credential.email,
			password: Config.Credential.password
		}, 
		function(err, api)
		{
			if (!err)
			{
				var bot = new PlugSuBot(api,Config,Language);
				api.on('roomJoin', function(room){bot.onRoomJoin(room);});
			} 
			else
			{
				console.log('Error initializing plugAPI: ' + err);
				setTimeout(function(){PlugSuBotInit();},1000*60);
			}
		}
	);
}

//--------------------------------------------------------------------------
//Init
//--------------------------------------------------------------------------
exports.run = function()
{
	PlugSuBotNextGen();
}