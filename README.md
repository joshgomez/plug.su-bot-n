# README #
## Plug.Su Bot Next Gen ##
* Command list with paging.
* Command manuals.
* Support many languages.
* Skip in **x** seconds if the song is **x** seconds too long.
* Reply if the song have already been played.
* Auto woot and meh when song is too long or have already been played. 
* Announce last played song along with the room score.
* Announce users who grab the song.
* Chatter.
* Join/Leaving and Welcome message.
* Auto messages.
* Question and answers.
* Random Chuck Norris jokes.
* Random cat facts.
* Ads.

### What is this repository for? ###

* Plug.Su Bot Next Gen source code
* 1.3.1

### How do I get set up? ###

**Configuration**

    Open "Config.json" and enter your Plug.dj credentials and the room's slug name.
    Install https://nodejs.org/en/ if you don't have it and then open terminal/command window in the "package.json" folder and type "npm install".
    
    For windows users hold down shift and press right mouse button and click on "Open command window here" in correct folder.

**Dependencies**

    fs
    path
    request
    cleverbot-node
    plugapi

**Deployment instructions**

    To run the bot type "node app" in the terminal/command window from the "app.js" folder.

**Translate**

    Open localization directory and make a copy of "*.en.json" file and rename it to your desired language. Open the file with a text editor and start translate.
    Remember to also change language code inside the file too. To use the new language open the "Config.json" and change the language value.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
* www.xuniver.se